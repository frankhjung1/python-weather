#!/usr/bin/env make

.PHONY: all badge clean cleanall doc format help lint report run test

.DEFAULT_GOAL	:= default

COVERAGE	:= 90	# minimum percentage for code coverage
CTAGS		:= $(shell which ctags)
PIP		:= $(shell which pip3)
PYTHON		:= $(shell which python3)
RM		:= $(shell which rm) -rf

PROJECT		:= weather
SRCS		:= $(wildcard $(PROJECT)/*.py $(PROJECT)/tests/*.py)

all:	check test report run
default: format preen test

help:
	@echo
	@echo "Default goal: ${.DEFAULT_GOAL}"
	@echo
	@echo "  all:    style and test"
	@echo "  preen:  format and lint"
	@echo "  format: format code, sort imports and requirements"
	@echo "  lint:   check code"
	@echo "  test:   run unit tests"
	@echo "  doc:    document module"
	@echo "  clean:  delete all generated files"
	@echo
	@echo "Initialise virtual environment (.venv) with:"
	@echo
	@echo "  pip3 install -U virtualenv; python3 -m virtualenv .venv; source .venv/bin/activate; pip3 install -Ur requirements.txt"
	@echo
	@echo "Start virtual environment (.venv) with:"
	@echo
	@echo "  source .venv/bin/activate"
	@echo
	@echo Deactivate with:
	@echo
	@echo "  deactivate"
	@echo

preen:	format tags lint

format:
	@ruff format
	@sort-requirements requirements.txt

lint:
	@ruff check --output-format grouped --fix
	@sort-requirements --check requirements.txt

lint_unsafe:
	@ruff check --unsafe-fixes --fix --show-fixes

tags:
ifdef CTAGS
	@ctags --recurse -o tags $(SRCS)
endif


test: preen
	@pytest --verbose --cov-fail-under=$(COVERAGE) $(PROJECT)

run:
	@python3 -m $(PROJECT)

report:	doc badge

doc:
	@pdoc $(PROJECT) !$(PROJECT).tests -o public
	@pytest --cov --cov-report=html:public/coverage \
	  --html=public/pytest_report.html --self-contained-html

badge:
	@pytest --junitxml=public/pytest_report.xml
	@bandit --configfile .bandit.yaml --recursive \
	  --format html --output public/bandit_report.html $(PROJECT)
	@genbadge tests --input-file public/pytest_report.xml --output-file public/tests.svg

clean:
	# clean generated artefacts
	$(RM) $(PROJECT)/__pycache__/ $(PROJECT)/*/__pycache__/
	$(RM) .coverage
	$(RM) .hypothesis/
	$(RM) .mypy_cache/
	$(RM) .pytest_cache/
	$(RM) .ruff_cache/
	$(RM) public/
	$(RM) tags

cleanall: clean
	# clean development environment
	$(RM) .idea/
	$(RM) .venv/
