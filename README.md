# Get Weather Forecast

A small Python utility to display weather forecast for a city.

It makes a call to <https://www.weatherapi.com/>. This requires a API key.
The key is read from the environment.

## Build

To get build instructions, call

```bash
make help
```

The default `make` target will build and test the application.

## Test

Test module using [pylint](https://www.pylint.org/):

```bash
pytest -v weather
```

To test the "bad" scenarios use:

```bash
pytest -v -k bad weather
```

To test using a selection of keys:

```bash
pytest -v -k "environment or render" weather
```

## Run

Show the weather for Adelaide, Australia.

```bash
export WEATHER_API_KEY=<your key>
python3 -m weather -c adelaide
```

This should return something like:

```text
Weather for Adelaide, South Australia, Australia
---
Current temperature 17.0°C
Current wind speed 22.0 km/h
Precipitation today 0.4 mm
Humidity 83%
UV index 4.0
```

## Documentation

Online weather documentation and reports at:

* [GitLab Pages](https://frankhjung1.gitlab.io/python-weather/weather.html)
* [Unit Test](https://frankhjung1.gitlab.io/python-weather/pytest_report.html)
* [Code Coverage](https://frankhjung1.gitlab.io/python-weather/coverage/index.html)

## Resources

* Security checks using [bandit](https://bandit.readthedocs.io/en/latest/)
* Build using [GNU Make](https://www.gnu.org/software/make/)
* Call API using [requests](https://requests.readthedocs.io/en/master/)
* Call weatherAPI from [weatherapi.com](https://weatherapi.com/)
* Format and lint tool [ruff](https://docs.astral.sh/ruff/)
* Organise requirements with [sort-requirements](https://pypi.org/project/sort-requirements/)
* Sort imports using [isort](https://pycqa.github.io/isort/)
* Unit test with [pytest](https://docs.pytest.org/en/latest/)
* Render weather data using a [Jinja2](https://jinja.palletsprojects.com/) template
