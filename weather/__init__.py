"""
A simple example on how to use a module to call a weather API.

## Reports

- [Code Coverage](./coverage/index.html)
- [Security Scan](./bandit_report.html)
- [Unit Tests](./pytest_report.html)
"""
