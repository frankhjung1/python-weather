"""
Call weather API to get forecast for given city.
"""

import requests


def get(city: str, key: str) -> dict[str, str]:
    """
    Get weather a city from the weather API.

    Parameters
    ----------

    city: str
        Name of city to get weather data for.

    key: str
        API key for weather API.

    Returns
    -------

    dict[str,str]:
        Dictionary containing weather and location data from the API.

    Raises
    ------

    HTTPError:
        If request returns a non-2xx status code.
    """

    uri = "https://api.weatherapi.com/v1/current.json"
    query = f"?key={key}&q={city}&aqi=no"
    req = requests.get(uri + query, timeout=20)

    # Raise exception for non-2xx status codes
    req.raise_for_status()

    # Extract location and current weather data from response
    weather = req.json()
    location = weather["location"]
    current = weather["current"]

    # Rename "name" key to "city" in location
    location["city"] = location.pop("name")

    # Merge location and current dictionaries
    return {**location, **current}
