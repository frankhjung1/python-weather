"""
Test the service using a mock API call.
"""

from unittest.mock import Mock

import pytest
from pytest_mock import MockerFixture
from requests import HTTPError, Response

from weather import service


@pytest.fixture(name="good_request")
def fixture_good_request_response() -> dict[str, dict[str, str]]:
    """Mock response from requests.get()."""
    return {
        "location": {
            "name": "London",
        },
        "current": {
            "temp_c": "18",
        },
    }


@pytest.fixture(name="good_service")
def fixture_good_service_response() -> dict[str, str]:
    """Expected dictionary from a good service.get() call."""
    return {
        "city": "London",
        "temp_c": "18",
    }


def test_good_get(good_request: str, good_service: str, mocker: MockerFixture):
    """Test good service.get() api call returns JSON weather forecast."""
    mocker.patch(
        "requests.get",
        return_value=Mock(status_code=200, json=lambda: good_request),
    )
    response = service.get("London", "mock_key")
    assert response == good_service


def test_bad_get(mocker: MockerFixture):
    """Test bad service.get() api call raises HTTPError."""
    mock_response = mocker.Mock(Response)
    mock_response.raise_for_status.side_effect = HTTPError()
    with pytest.raises(HTTPError):
        service.get("London", "mock_key")
