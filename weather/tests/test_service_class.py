"""
Test get weather service.

This is an unit test example using the
[unittest.mock](https://docs.python.org/3/library/unittest.mock.html) library.

Compare this to the [pytest.mock](https://pypi.org/project/pytest-mock/)
library that is a wrapper to `unittest.mock`.
"""

import unittest
from unittest import mock

from requests import HTTPError

from weather import service


class TestGet(unittest.TestCase):
    """Mock Service API calls."""

    @mock.patch("requests.get")
    def test_good_get(self, mock_get):
        """Test good service.get() api call."""
        # Set up mock response
        mock_response = {
            "location": {
                "name": "London",
            },
            "current": {
                "temp_c": "18",
            },
        }
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.status_code = 200

        # Call get() with mock parameters
        result = service.get("London", "mock_key")

        # Check that requests.get() was called with the correct parameters
        uri = "https://api.weatherapi.com/v1/current.json"
        query = "?key=mock_key&q=London&aqi=no"
        mock_get.assert_called_once_with(uri + query, timeout=20)

        # Check that the result is the expected dictionary
        expected_result = {
            "city": "London",
            "temp_c": "18",
        }
        self.assertEqual(result, expected_result)

    @mock.patch("requests.get")
    def test_bad_get(self, mock_get):
        """Test bad service.get() api call."""
        exception = HTTPError(mock.Mock(status=404), "not found")
        mock_get(mock.ANY).raise_for_status.side_effect = exception
        with self.assertRaises(HTTPError):
            service.get("London", "mock_key")
