"""
Run unit tests for rendering function.
"""

import pytest

from weather import render


@pytest.fixture(name="good_data")
def fixture_good_data() -> dict[str, str]:
    """Weather test data."""
    return {
        "city": "Adelaide",
        "region": "South Australia",
        "country": "Australia",
        "temp_c": "32",
        "wind_kph": "100",
        "precip_mm": "125",
        "humidity": "89",
        "uv": "100",
    }


@pytest.fixture(name="expected_output")
def fixture_expected_output() -> str:
    """Expected rendering of weather data."""
    return """Weather for Adelaide, South Australia, Australia
---
Current temperature 32°C
Current wind speed 100 km/h
Precipitation today 125 mm
Humidity 89%
UV index 100"""


@pytest.fixture(name="good_template")
def fixture_good_template() -> str:
    """Path to a good template."""
    return "templates/text.jinja"


@pytest.fixture(name="bad_template")
def fixture_bad_template() -> str:
    """A bad template path."""
    return "templates/no_such_template.jinja"


def test_good_template(
    good_data: dict[str, str], expected_output: str, good_template: str
):
    """Check output data for a good template."""
    result = render.show(good_data, good_template)
    assert result == expected_output


def test_bad_template(good_data: dict[str, str], bad_template: str):
    """Check rendering fails when given a bad template."""
    with pytest.raises(FileNotFoundError):
        render.show(good_data, bad_template)
