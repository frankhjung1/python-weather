"""
Run unit tests for environment function.

Note: this will overwrite the environment variable `WEATHER_API_KEY`.
"""

import os

import pytest

from weather import environment


def setup():
    """Prepare environment for testing."""
    os.environ["WEATHER_API_KEY"] = "test_key"


def teardown():
    """Clean up environment after testing."""
    if "WEATHER_API_KEY" in os.environ:
        os.environ.pop("WEATHER_API_KEY")


def test_good_environment():
    """Can get key value from environment."""
    try:
        environment.get_key()
    except KeyError as exception:
        pytest.fail(f"Unexpected KeyError {exception}")


def test_bad_environment():
    """Missing environment raises exception."""
    os.environ.pop("WEATHER_API_KEY")
    with pytest.raises(KeyError):
        environment.get_key()
