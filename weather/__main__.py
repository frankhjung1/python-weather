"""
Retrieve the weather forecast for a given city. This will call the
<https://www.weatherapi.com/ Weather API>. The program requires that the
`WEATHER_API_KEY` environment variable be set.
"""

import argparse
import logging
import sys

from requests import HTTPError

from weather import environment, render, service

__version__ = "2025-02-07"

arg_parser = argparse.ArgumentParser(
    usage="weather [options]",
    description="Show weather for the specified city."
    + "Requires an API key from www.weatherapi.com named WEATHER_API_KEY.",
    epilog="© 2023 Frank Jung mailto: frankhjung@linux.com",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
arg_parser.add_argument(
    "-c",
    "--city",
    type=str,
    help="name of city to get weather forecast",
    default="melbourne",
)
arg_parser.add_argument(
    "-t",
    "--template",
    type=str,
    help="render forecast using this template",
    default="templates/text.jinja",
)
arg_parser.add_argument(
    "-v",
    "--version",
    help="show version",
    action="version",
    version=__version__,
)
arg_parser.add_argument("-d", "--debug", action="store_true", help="show debug output")

# get city from the command line parameters
args = arg_parser.parse_args()

# set up logging
logging.basicConfig(
    stream=sys.stdout,
    format="%(asctime)s [%(levelname)s] %(module)s: %(message)s",
    level=logging.INFO,
)
logger = logging.getLogger()
if args.debug:
    logger.setLevel(logging.DEBUG)
    logger.debug("Debug output enabled")

# read the weather data from the API and display it.
try:
    API_KEY = environment.get_key()
    WEATHER_DATA = service.get(args.city, API_KEY)
    print(render.show(WEATHER_DATA, args.template))
except (
    KeyError,  # api key not found
    HTTPError,  # api request failed
    FileNotFoundError,  # template not found
) as exception:
    logger.error(exception)
    sys.exit(1)
