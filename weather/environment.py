"""
Read API key, `WEATHER_API_KEY` from environment.
"""

import os


def get_key() -> str:
    """
    Read API key, `WEATHER_API_KEY` from environment.

    Returns
    -------

    str:
        API key.

    Raises
    ------

    KeyError:
        If `WEATHER_API_KEY` is not set.
    """
    api_key = os.getenv("WEATHER_API_KEY")
    if api_key is None:
        raise KeyError("missing value for `WEATHER_API_KEY`")
    return api_key
