"""
Render the weather data using a Jinja template.
"""

from jinja2 import Template


def show(
    data: dict[str, str], template_path: str, context_variable: str = "weather"
) -> str:
    """
    Uses a Jinja template to display weather data.

    Parameter
    ---------

    weather_data: dict[str, str]
        Dictionary containing weather and location data from the API.

    template: str
        Path to the Jinja2 template

    Returns
    -------

    str:
        Formatted string containing the weather forecast.
    """
    with open(template_path, mode="rt", encoding="UTF-8") as file:
        template = Template(file.read())
        return template.render(**{context_variable: data})
